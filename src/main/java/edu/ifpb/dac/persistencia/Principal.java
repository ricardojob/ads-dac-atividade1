package edu.ifpb.dac.persistencia;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Ricardo Job
 */
public class Principal {

    public static void main(String[] args) {
        Pessoa pessoa = new Pessoa(1, "Kiko", "3256988");
        Pessoa pessoa2 = new Pessoa(3, "Fulano", "12345");

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("atividade1");
        EntityManager em = emf.createEntityManager();

        salvar(em, pessoa);
        salvar(em, pessoa2);
        
        List<Pessoa> lista = listar(em);

        for (Pessoa pessoa1 : lista) {
            System.out.println(pessoa1);
        }
 
        
    }

    private static void salvar(EntityManager em, Pessoa pessoa) {
        em.getTransaction().begin();
        try {
            em.persist(pessoa);
            em.getTransaction().commit();
            System.out.println("Salvou!!!!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    private static List<Pessoa> listar(EntityManager em) {
        return em.createQuery("Select p From Pessoa p").getResultList();
    }
}
